<!-- ---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
-->

# Git Upstream Tracker

Tool to track single source files included from one or more upstream projects into one downstream project. Useful both for compliance (REUSE compliance, SBOM generation) and for development purposes (keep track of upstream updates).

## Context and Rationale

Oniro project meta-layers contain a number of recipe and patch files that have been cherry-picked from different upstream repositories (poky, meta-openembedded, meta-virtualization, etc.) and/or from different revisions of the same upstream repositories.

This leads to two issues:

- files copied from upstream usually do not have any REUSE/SPDX tag inside, so manual work is needed to reconstruct copyright and license metadata
- manual work is needed to check whether copied files (especially recipes) have been updated upstream (eg. for CVE fixes etc.)

Manual work is time-consuming and error-prone, so it cannot be a viable solution in the long run.

Git Upstream Tracker aims at solving the above issues, by performing automatic upstream tracking of cherry-picked files by inspecting downstream and upstream repos through git commands and by generating a csv report that can be used for further processing, containing the following information:

| sha1sum | path | upstream_uri | upstream_commit | upstream_commit_date | upstream_path | upstream_new_rev | upstream_new_rev_date | upstream_branch |
| ------- | ---- | ------------ | --------------- | -------------------- | ------------- | ---------------- | --------------------- | --------------- |
| 1b05a86efd1d47e232eb2af9782abbec45bd9115	| meta-oniro-staging/recipes-devtools/binutils/binutils-2.39.inc |	https://git.yoctoproject.org/poky |	d9c8d84f2b4680ad7303460257f98810dc57d379	| 2022-08-21 21:51:43	| meta/recipes-devtools/binutils/binutils-2.39.inc	| 72d141568694ac4c2f9ac0919a4c791754fdb4d2 |	2022-09-22 11:11:49	| langdale;master;master-next |


## Requirements

- git
- [reuse tool](https://github.com/fsfe/reuse-tool)

Recommended way for installing reuse tool:

```
sudo apt get python3-venv pipx
pipx install reuse
```

## Usage

```
usage: git_upstream_tracker.py [-h] --filter-type {reuse,all} --csv-output
                               CSV_OUTPUT --needle NEEDLE --haystack HAYSTACK
                               [HAYSTACK ...]

optional arguments:
  -h, --help            show this help message and exit
  --filter-type {reuse,all}
                        search filter in needle repo; 'reuse': only files with
                        missing REUSE headers; 'all': all files
  --csv-output CSV_OUTPUT
                        csv filename where to save results
  --needle NEEDLE       repo with cherrypicked files from upstream repos (if
                        it's a http or git uri it will be fetched; if it's a
                        local directory path, it will be updated)
  --haystack HAYSTACK [HAYSTACK ...]
                        upstream repo(s) where to search for matches (if it's a
                        http or git uri it will be fetched; if it's a local
                        directory path, it will be updated)
```

### Example 1

In this example, needle and haystack repos are fetched locally in a temporary directory, that gets deleted after generating the csv report

```bash
git_upstream_tracker.py \
  --filter-type reuse \
	--csv-output report.csv \
  --needle https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git \
  --haystack \
    https://git.openembedded.org/meta-openembedded \
    https://git.yoctoproject.org/poky \
    https://git.yoctoproject.org/meta-virtualization \
    https://git.yoctoproject.org/meta-arm
```

### Example 2

In this example, needle and haystack repos have already been fetched locally, and we want to use local clones instead of re-fetching everything.

```bash
git_upstream_tracker.py \
  --filter-type reuse \
	--csv-output report.csv \
  --needle ./downstream/oniro \
  --haystack \
    ./upstream/meta-openembedded \
    ./upstream/poky \
    ./upstream/meta-virtualization \
    ./upstream/meta-arm
```

## How it works

First, a list of needle files is generated from the "needle" repo, based on `--filter-type` value: with "reuse", only files with missing REUSE/SPDX headers will be selected; with "all", all files will be selected.

Then for every selected needle file the corresponding git object hash is calculated, in order to search for that hash in all haystack repositories' trees and branches. In case of multiple matches in different commits within the same haystack repo, a rule of thumb based on commit count is used to select only one commit, which should likely be the commit from which all (or most) upstream files have been cherrypicked.

For every match, git log command is used to check if the upstream file has been modified after cherry-picking.
