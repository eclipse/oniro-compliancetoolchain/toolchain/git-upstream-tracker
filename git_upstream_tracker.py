#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>


import os
import re
import csv
import argparse
import tempfile
import subprocess
from datetime import datetime


def bash(cmd, cwd=None, ignore_errors=False):
    try:
        return subprocess.check_output(cmd, cwd=cwd, shell=True, executable="/bin/bash").decode("utf-8").strip("\n")
    except subprocess.CalledProcessError as e:
        if ignore_errors:
            return e.output.decode("utf-8").strip("\n")
        raise e

def git_clone(repo_uri, directory, all_branches=False):
    if repo_uri.startswith("https://") or repo_uri.startswith("git://"):
        print(f"cloning {repo_uri} in {directory}")
        bash(f"git clone {repo_uri}", cwd=directory)
        git_dir = os.path.join(
            directory,
            re.sub(r'\.git$', '', os.path.basename(repo_uri))
        )
    else:
        print(f"updating {repo_uri}")
        git_dir = os.path.realpath(repo_uri)
        bash("git pull", cwd=git_dir)
    if all_branches:
        print(f"fetching all branches of {repo_uri}")
        out = bash("git branch -r | grep -v '\->'", cwd=git_dir)
        branches = out.split()
        for remote in branches:
            local = re.sub(r'^origin/', '', remote)
            try:
                bash(f"git branch --track {local} {remote}", cwd=git_dir)
            except subprocess.CalledProcessError as e:
                if e.returncode != 128: # branch already exists
                    raise e
        bash("git fetch --all && git pull --all", cwd=git_dir)
    return git_dir

def get_all_files(git_dir):
    print(f"getting all files in {git_dir}")
    out = bash("find . -type f -not -path './.git/*'", cwd=git_dir)
    return [ re.sub(r'^\./','', f) for f in out.split()  ]

def get_missing_reuse_files(git_dir):
    missing = []
    print(f"getting reuse tool output for {git_dir}")
    bash("rm -Rf .reuse", cwd=git_dir, ignore_errors=True) # we want to look for SPDX tags only
    out = bash("LANG=en_US.UTF-8 reuse lint", cwd=git_dir, ignore_errors=True)
    to_add = False
    for line in out.split('\n'):
        if line == "The following files have no copyright and licensing information:":
            to_add = True
            continue
        elif to_add:
            if line:
                missing.append(re.sub(r'^\* ', '', line))
            else:
                to_add = False
                break
    return missing

def get_files(git_dir, filter_type):
    if filter_type == "all":
        files = get_all_files(git_dir)
    elif filter_type == "reuse":
        files = get_missing_reuse_files(git_dir)
    print("getting git object hash and sha1 checksum for every file")
    return {
        path: {
            "git_obj_hash": bash(f"git hash-object {path}", cwd=git_dir),
            "sha1sum": bash(f'sha1sum {path} | cut -d" " -f 1', cwd=git_dir),
            "matches": {}
        }
        for path in files
    }

def find_matches(needle_git_obj_hash, haystack_git_dirs):
    matches = {}
    for haystack_git_dir in haystack_git_dirs:
        out = bash("git remote -v | tail -1", cwd=haystack_git_dir)
        _, uri, _ = out.split(maxsplit=2)
        found_commits = []
        print(f"\tsearching for git object {needle_git_obj_hash} in {uri}")
        out = bash(
            f"git log --all --pretty=tformat:'%ct %T %H %s' --find-object={needle_git_obj_hash}",
            cwd=haystack_git_dir
        )
        if not out:
            continue
        for line in out.split('\n'):
            timestamp, tree, commit, comment = line.split(maxsplit=3)
            out = bash(f"git branch --contains {commit}", cwd=haystack_git_dir)
            branches = [ b for b in out.split() if b != '*' ]
            print(f"\t\tsearching in tree {tree}")
            out = bash(
                f"git ls-tree -r {tree} | grep {needle_git_obj_hash}",
                cwd=haystack_git_dir, ignore_errors=True
            )
            if not out:
                continue
            paths_in_haystack = []
            available_new_revs = []
            new_rev_dates = []
            for line1 in out.split('\n'):
                _, _, _, path_in_haystack = line1.split(maxsplit=3)
                paths_in_haystack.append(path_in_haystack)
                out = bash(
                    f"git log -n 1 --diff-filter=dr --all --pretty=tformat:'%ct %H' -- {path_in_haystack}",
                                   # ^ --diff-filter=dr exludes commits in which the file has been deleted/renamed
                                   # (otherwise it may return a commit where the file doesn't exist any more
                                   # and the next git rev-parse command would return error)
                    cwd=haystack_git_dir
                )
                last_timestamp, last_commit = out.split()
                last_timestamp = int(last_timestamp)
                last_git_obj_hash = bash(
                    f"git rev-parse {last_commit}:{path_in_haystack}",
                    cwd=haystack_git_dir
                )
                if last_git_obj_hash != needle_git_obj_hash:
                    available_new_revs.append(last_commit)
                    new_rev_dates.append(
                        str(datetime.fromtimestamp(last_timestamp))
                    )
            matches.update({
                f"{uri}@{commit}": {
                    "uri": uri,
                    "commit": commit,
                    "timestamp": int(timestamp),
                    "paths": paths_in_haystack,
                    "available_new_revs": available_new_revs,
                    "new_rev_dates": new_rev_dates,
                    "branches": branches
                }
            })
            print(f"\t\tfound in commit {commit}")
    return matches

def clone_needle_and_haystack_repos(tmpdir, needle_repo, haystack_repos):
    needle_dir = os.path.join(tmpdir, "needle")
    haystack_dir = os.path.join(tmpdir, "haystack")
    os.mkdir(needle_dir)
    os.mkdir(haystack_dir)
    needle_git_dir = git_clone(needle_repo, needle_dir)
    haystack_git_dirs = [
        git_clone(haystack_repo, haystack_dir, all_branches=True)
        for haystack_repo in haystack_repos
    ]
    return needle_git_dir, haystack_git_dirs

def get_commit_stats(needle_files):
    commit_stats = {}
    for f, fdata in needle_files.items():
        if not fdata["matches"]:
            continue
        for m, mdata in fdata["matches"].items():
            if not commit_stats.get(m):
                commit_stats[m] = {
                    "count": 1,
                    "timestamp": mdata["timestamp"]
                }
            else:
                commit_stats[m]["count"] += 1
    return commit_stats

def get_best_commit(commit_list, commit_stats):
    """when a file/object is found in multiple commits, try to guess the "best"
    one based on a rule of thumb: if many other searched objects are found in
    the same commit, it is likely that developer(s) cherry-picked all files from
    that commit, so we choose the commit with highest count in stats; in case
    the count is the same, we choose the most recent one, based on timestamp"""
    max_count = 0
    candidates = []
    for commit in commit_list:
        if commit_stats[commit]["count"] > max_count:
            candidates = [ commit, ]
            max_count = commit_stats[commit]["count"]
        elif commit_stats[commit]["count"] == max_count:
            candidates.append(commit)
    if len(candidates) == 1:
        return candidates[0]
    max_timestamp = 0
    for candidate in candidates:
        if commit_stats[candidate]["timestamp"] > max_timestamp:
            better_candidate = candidate
            max_timestamp = commit_stats[candidate]["timestamp"]
    return better_candidate

def get_matches(needle_files, haystack_git_dirs):
    print("getting matches")
    for path, data in needle_files.items():
        print(f"looking for {path}")
        data["matches"] = find_matches(data["git_obj_hash"], haystack_git_dirs)

def clean_matches(needle_files):
    print("cleaning matches")
    commit_stats = get_commit_stats(needle_files)
    for f, fdata in needle_files.items():
        if not fdata["matches"]:
            fdata["match"] = None
        else:
            commit_list = fdata["matches"].keys()
            best_match = get_best_commit(commit_list, commit_stats)
            fdata["match"] = fdata["matches"][best_match]
        del(fdata["matches"])

def find_needles(needle_repo, haystack_repos, filter_type):
    with tempfile.TemporaryDirectory() as tmpdir:
        needle_git_dir, haystack_git_dirs = clone_needle_and_haystack_repos(tmpdir, needle_repo, haystack_repos)
        needle_files = get_files(needle_git_dir, filter_type)
        get_matches(needle_files, haystack_git_dirs)
        clean_matches(needle_files)
    return needle_files

def create_csv_table(needle_files):
    print("creating csv table")
    csv_table = []
    for path, data in needle_files.items():
        row = {
            "sha1sum": data["sha1sum"],
            "path": path,
            "upstream_uri": "",
            "upstream_commit": "",
            "upstream_commit_date": "",
            "upstream_path": "",
            "upstream_new_rev": "",
            "upstream_new_rev_date": "",
            "upstream_branch": ""
        }
        if data["match"]:
            row["upstream_uri"] = data["match"]["uri"]
            row["upstream_commit"] = data["match"]["commit"]
            row["upstream_commit_date"] = str(datetime.fromtimestamp(data["match"]["timestamp"]))
            row["upstream_path"] = ";".join(data["match"]["paths"])
            row["upstream_new_rev"]  = ";".join(data["match"]["available_new_revs"])
            row["upstream_new_rev_date"]  = ";".join(data["match"]["new_rev_dates"])
            row["upstream_branch"] = ";".join(data["match"]["branches"])
        csv_table.append(row)
    return csv_table

def write_csv_table(csv_table, csv_path):
    print(f"writing csv_table to {csv_path}")
    with open(csv_path, 'w') as f:
        w = csv.DictWriter(f, csv_table[0].keys())
        w.writeheader()
        for row in csv_table:
            w.writerow(row)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(conflict_handler='resolve')
    parser.add_argument(
        "--filter-type",
        choices = [ "reuse", "all"],
        required = True,
        help = "search filter in needle repo; 'reuse': only files with missing REUSE/SPDX headers; 'all': all files"
    )
    parser.add_argument(
        "--csv-output",
        type = str,
        required = True,
        help = "csv filename where to save results"
    )
    parser.add_argument(
        "--needle",
        type = str,
        required = True,
        help = "repo with cherrypicked files from upstream repos (if it's a http or git uri it will be fetched; if it's a local directory path, it will be updated)"
    )
    parser.add_argument(
        "--haystack",
        type = str,
        nargs = "+",
        required = True,
        help = "upstream repo(s) where to search for matches (if it's a http or git uri it will be fetched; if it's a local directory path, it will be updated)"
    )
    args = parser.parse_args()
    print(f"\nsearching {args.needle} files in {args.haystack} with filter '{args.filter_type}'"
    f" and writing results to {args.csv_output}\n")
    needle_files = find_needles(args.needle, args.haystack, args.filter_type)
    csv_table = create_csv_table(needle_files)
    write_csv_table(csv_table, args.csv_output)
